<?php

use App\Models\Design;
use Illuminate\Database\Seeder;

class DesignTableSeeder extends Seeder
{
    public function run(Faker\Generator $faker)
    {
        for ($i = 0; $i < 200; $i++) {
            $design = Design::firstOrNew([
                'internal_id' => ($internal_id = $i + 1),
            ]);

            if (!$design->exists) {
                $design->fill([
                    'name' => ($name = $faker->words(2, true)),
                    'preview_url' => 'https://design'.$internal_id.'.horoshop.ua',
                ])->save();

                $design->addStorageFile('designs/item'.($i % 4 + 1).'.png');
            }
        }
    }
}
