# Installation

Run commands:

`composer install && npm install`

`npm run dev` or `npm run prod`

`cp .env.example .env` and setup environment

`php artisan key:generate`

`php artisan migrate --seed`

`php artisan serve` and navigate http://127.0.0.1:8000/
