require('./bootstrap');

window.Vue = require('vue');

Vue.component('image-uploader', require('./components/ImageUploader').default);

const app = new Vue({
    el: '#app',
});
