@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col">
                <h2 class="mb-4">all designs</h2>
            </div>
            <div class="col text-right">
                <a href="{{ route('design.create') }}" class="btn btn-primary">
                    Create new
                </a>
            </div>
        </div>

        <div class="text-decoration-none d-flex flex-wrap justify-content-between">
            @foreach($designs as $design)
                <a href="{{ route('design.edit', $design) }}" class="mb-5 scale-1-hover text-decoration-none transition-default z-index-1000">
                    <div style="min-width: 200px; min-height: 300px;">
                        @if($image = $design->images->first())
                            <img class="rounded" width="200" height="300" style="object-fit: cover" src="{{ $image->url }}" alt="{{ $image->alt }}"/>
                        @endif
                    </div>
                    <div class="mt-3">
                        <div class="align-middle badge rounded-pill pl-2 pr-2 badge badge-primary d-inline">{{ $design->internal_id }}</div>
                        <div class="align-middle d-inline font-weight-bold ml-1 fs-2">{{ $design->name }}</div>
                    </div>
                </a>
            @endforeach
        </div>
        <div class="d-flex justify-content-center">
            {{ $designs->links() }}
        </div>
    </div>
@endsection
