@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col">
                <h2 class="mb-4">
                    <a href="{{ route('design.index') }}">&lt; all designs</a>
                    Design {{ $design ? 'editing' : 'creation' }}
                </h2>
            </div>
            <div class="col text-right">
                @if($design)
                    <form method="post" action="{{ route('design.destroy', $design) }}" class="d-inline">
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger" value="Delete"/>
                    </form>
                @endif
                <input type="submit" form="mainForm" class="btn btn-primary" value="Save"/>
            </div>
        </div>

        <form id="mainForm" method="post" action="{{ $design ? route('design.update', $design) : route('design.store') }}">
            @csrf
            @if($design) @method('PATCH') @endif

            <div class="row">
                <div class="col-2">
                    <input type="text" class="form-control" placeholder="ID" name="internal_id"
                           value="{{ old('internal_id', $design->internal_id ?? '') }}"/>
                </div>
                <div class="col-5">
                    <input type="text" class="form-control" placeholder="Name" name="name"
                           value="{{ old('name', $design->name ?? '') }}"/>
                </div>
                <div class="col-5">
                    <input type="text" class="form-control" placeholder="Preview URL" name="preview_url"
                           value="{{ old('preview_url', $design->preview_url ?? '') }}"/>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-12">
                    <image-uploader :images="{{ $design ? $design->images->map(fn($e)=>$e->only(['id', 'src', 'url'])) : '{}' }}"/>
                </div>
            </div>
        </form>
    </div>
@endsection
