@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>
                    <div class="card-body">
                        <a href="{{ route('design.index') }}" class="btn btn-primary">Designs</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
