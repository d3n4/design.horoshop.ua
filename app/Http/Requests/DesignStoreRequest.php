<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DesignStoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'internal_id' => 'required|numeric|unique:designs,internal_id',
            'name' => 'required',
            'preview_url' => 'required',

            'images' => 'array',
            'images.*' => 'exists:images,id',
        ];
    }
}
