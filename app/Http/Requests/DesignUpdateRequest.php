<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Route;

class DesignUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'internal_id' => 'required|numeric|unique:designs,internal_id,'.Route::current()->parameter('design')->id,
            'name' => 'required',
            'preview_url' => 'required',

            'images' => 'array',
            'images.*' => 'exists:images,id',
        ];
    }
}
