<?php

namespace App\Http\Controllers;

use App\Http\Requests\UploadImageRequest;
use App\Models\Image;

class ImageController extends Controller
{
    public function upload(UploadImageRequest $request)
    {
        $upload = $request->file('file')->store('images', 'public');
        $image = Image::create([
            'src' => $upload,
        ]);
        return [
            'success' => true,
            'id' => $image->id,
        ];
    }
}
