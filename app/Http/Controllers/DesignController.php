<?php

namespace App\Http\Controllers;

use App\Http\Requests\DesignDestroyRequest;
use App\Http\Requests\DesignEditRequest;
use App\Http\Requests\DesignStoreRequest;
use App\Http\Requests\DesignUpdateRequest;
use App\Models\Design;
use Exception;

class DesignController extends Controller
{
    public function show(Design $design)
    {
        throw new Exception('not implemented');
    }

    public function index()
    {
        return view('design.index', [
            'designs' => Design::with('images')->paginate()
        ]);
    }

    public function create()
    {
        return view('design.create-edit', ['design' => null]);
    }

    public function store(DesignStoreRequest $request)
    {
        $design = Design::create($request->only(['internal_id', 'name', 'preview_url']));
        $design->images()->sync($request->input('images', []));
        return redirect()->route('design.edit', $design)->withMessage('Design successfully created');
    }

    public function edit(DesignEditRequest $request, Design $design)
    {
        return view('design.create-edit', compact('design'));
    }

    public function update(DesignUpdateRequest $request, Design $design)
    {
        $design
            ->fill($request->only(['internal_id', 'name', 'preview_url']))
            ->save();
        $design->images()->sync($request->input('images', []));
        return redirect()->back()->withMessage('Design successfully saved');
    }

    public function destroy(DesignDestroyRequest $request, Design $design)
    {
        if ($design->delete()) {
            return redirect()->route('design.index')->with('message', 'Design #'.$design->id.' successfully destroyed');
        }

        return redirect()->route('design.index')->withErrors(['Something gone wrong']);
    }
}
