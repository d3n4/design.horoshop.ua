<?php

namespace App\Models\Traits;

use App\Models\Image;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use InvalidArgumentException;

trait Imageable
{
    public function getDefaultImageUrl()
    {
        if (!($image = $this->getDefaultImage())) {
            return null;
        }
        return $image->url;
    }

    public function getDefaultImage()
    {
        return $this->images()->first();
    }

    public function images(): MorphToMany
    {
        return $this->morphToMany(Image::class, 'imageable');
    }

    public function addStorageFile($src, $alt = null)
    {
        $image = Image::firstOrNew(compact('src'));
        if (!$image->exists) {
            $image->fill(compact('src', 'alt'))->save();
        }
        $this->addImage($image);
    }

    public function addImage(Image $image): void
    {
        $this->images()->attach($image);
    }

    public function addImages(iterable $images)
    {
        foreach ($images as $image) {
            if (!$image instanceof Image) {
                throw new InvalidArgumentException(
                    sprintf(
                        'Every element passed to addImages must be a Image object. Given `%s`.',
                        is_object($image) ? get_class($image) : gettype($image)
                    )
                );
            }
        }

        return $this->images()->saveMany($images);
    }

    public function removeImage(Image $image)
    {
        return $this->images()->detach($image);
    }
}
