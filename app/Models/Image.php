<?php

namespace App\Models;

use Storage;

class Image extends Model
{
    public $fillable = [
        'src',
        'alt'
    ];

    public function getUrlAttribute()
    {
        return Storage::url($this->src);
    }
}
