<?php

namespace App\Models;

use App\Models\Traits\Imageable;

class Design extends Model
{
    use Imageable;

    public $fillable = [
        'internal_id',
        'name',
        'preview_url',
    ];
}
